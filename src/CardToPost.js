import React from 'react';
import {Button} from "@mui/material";
import axios from 'axios';
import Placeholder from './Placeholder.png';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';

function CardToPost(props) {
    const { title, content, image_url } = props.post;
    
        return (
            <div>
                
                <Card sx={{ maxWidth: 230 }}>
                    <CardMedia
                        component="img"
                        height="140"
                        image="https://picsum.photos/200/300?random"
                        alt={Placeholder}
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h4" component="div">
                            {title}
                        </Typography>
                        <Typography variant="body2" color="text.secondary">
                            {content}
                        </Typography>
                    </CardContent>
                </Card>

            </div>
        )
    }

export default CardToPost;


