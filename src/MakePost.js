import React from 'react';
import axios from 'axios';
import {Button, MenuItem, Select} from "@mui/material";
import TextField from '@mui/material/TextField';
import './MakePost.css';

export default class MakePost extends React.Component {
     state = {
        naam: '', type: '', bericht: ''
    }


    handleChange1 = event => {
        this.setState({ naam: event.target.value });
    }

    handleChange2 = event => {
        this.setState({ type: event.target.value });
    }

    handleChange3 = event => {
        this.setState({ bericht: event.target.value });
    }

    handleSubmit = event => {
        event.preventDefault();

        const title = this.state.naam;

        const category_id = this.state.type;

        const content = this.state.bericht;

       // const { naam, type, bericht } = test;

        axios.post(`http://178.62.198.162/api/posts`, {title, category_id ,content}, {
            headers: {
            'token': `pj11daaQRz7zUIH56B9Z`
        },
        })

            .then(res => {
                console.log(res);
                console.log(res.data);
            })
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <h5>
                        Berichtnaam
                    </h5>

                    <TextField
                        style={{ backgroundColor: "#d8d8d8" }}
                        name="naam"
                        autoComplete="off"
                        placeholder={"Geen titel"}
                        value={this.state.naam}
                        onChange={this.handleChange1}
                    />

                    <h5>
                        Categorie
                    </h5>

                    <label>

                        <Select
                            style={{ backgroundColor: "#d8d8d8" }}
                            labelId="SelectLabel"
                            id="Select"
                            className={"TypeSelect"}
                            name={"type"}
                            onChange={this.handleChange2}
                            value={this.state.type}
                        >
                            <MenuItem value={"none"} disabled>
                                Geen categorie
                            </MenuItem>
                            <MenuItem value={1}>Tech</MenuItem>
                            <MenuItem value={2}>Nieuws</MenuItem>
                            <MenuItem value={3}>Sports</MenuItem>
                            <MenuItem value={4}>Lokaal</MenuItem>

                        </Select>
                    </label>

                    <h5>
                        Bericht
                    </h5>

                    <TextField
                        style={{ backgroundColor: "#d8d8d8" }}
                        multiline={true}
                        rows={16}
                        name="bericht"
                        autoComplete="off"
                        variant="outlined"
                        onChange={this.handleChange3}
                    > {this.state.bericht} </TextField>

                    <Button
                        sx={{
                            backgroundColor: '#CD7F32',
                            borderRadius: "35px"
                        }}
                        onClick={this.handleSubmit} variant="contained" className={"Button"}>Bericht aanmaken<
                    /Button>

                </form>
            </div>
        )
    }
}

