import React, { useState, useEffect } from 'react';
import {Button} from "@mui/material";
import axios from 'axios';
import Placeholder from './Placeholder.png';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import {Grid} from "@mui/material";
import CardToPost from "./CardToPost";


function ShowPost(props) {

    const [posts, setPosts] = useState([]);

    // Door de getData functie binnen een useEffect aan te roepen wordt de functie aangeroep zodra het component is geladen
    useEffect(() => {
        axios.get(`http://178.62.198.162/api/posts?page=1`, { headers: {
                    "token" : "pj11daaQRz7zUIH56B9Z"
                }
        }).then(res => {
                console.log(res);
                console.log(res.data);
                setPosts(res.data.slice(0,4));
            })
    }, []);
    
        return (

            <div>

                <Grid container spacing={2}>
                    {posts.map((post) => (
                        <Grid key={post.id} item xs={12} md={6}>
                            <CardToPost post={post} />
                            <p>{post.image_url}</p>
                        </Grid>
                    ))}

                </Grid>

            </div>
        )
}


export default ShowPost;

