import Banner from './Banner.png';
import './Banner.css';

function SocialBrothersLogo() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={Banner} alt={"banner"}/>
      </header>
    </div>
  );
}

export default SocialBrothersLogo;
