import React from 'react';
import ReactDOM from 'react-dom';
import SocialBrothersLogo from "./SocialBrothersLogo";
import './index.css';
import MakePost from "./MakePost";
import { Grid } from '@mui/material';
import ShowPost from "./ShowPost";


ReactDOM.render(
  <React.StrictMode>
    <SocialBrothersLogo />

      <Grid container spacing={5} mt={5} >

          <Grid item xs={5} mx={10} className={"MakePost"}>
             <MakePost />
          </Grid>

          <Grid item xs={5} className={"readPost"}>
              <ShowPost />

          </Grid>
      </Grid>
  </React.StrictMode>,
  document.getElementById('root')
);

